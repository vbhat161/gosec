package main

import (
	"testing"
)

func TestFileExistsAtPath(t *testing.T) {
	tests := []struct {
		name    string
		path    string
		wantErr bool
	}{
		{
			name:    "existing file",
			path:    "CHANGELOG.md",
			wantErr: false,
		},
		{
			name:    "existing nested file",
			path:    "plugin/plugin.go",
			wantErr: false,
		},
		{
			name:    "missing file",
			path:    "./boop",
			wantErr: true,
		},
		{
			name:    "existing directory",
			path:    "test",
			wantErr: true,
		},
	}

	for idx := range tests {
		tt := tests[idx]
		t.Run(tt.name, func(t *testing.T) {
			err := fileExistsAtPath(tt.path)
			if (err != nil) != tt.wantErr {
				t.Fatalf("fileExistsAtPath() for path: %v, unexpected error: %v", tt.path, err)
			}
		})
	}
}
